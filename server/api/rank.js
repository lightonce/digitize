import client from "../utils/prisma";

export default defineEventHandler(async () => {
	return client.country.findMany({
		take: 15,
		orderBy: {
			rank: "asc",
		}
	})
});
