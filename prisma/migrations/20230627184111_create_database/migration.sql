-- CreateTable
CREATE TABLE "Country" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "english_name" TEXT NOT NULL,
    "russian_name" TEXT NOT NULL,
    "rank" INTEGER NOT NULL,
    "technology" INTEGER NOT NULL,
    "knowledge" INTEGER NOT NULL,
    "readiness" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Country_english_name_key" ON "Country"("english_name");

-- CreateIndex
CREATE UNIQUE INDEX "Country_russian_name_key" ON "Country"("russian_name");

-- CreateIndex
CREATE UNIQUE INDEX "Country_rank_key" ON "Country"("rank");

-- CreateIndex
CREATE UNIQUE INDEX "Country_technology_key" ON "Country"("technology");

-- CreateIndex
CREATE UNIQUE INDEX "Country_knowledge_key" ON "Country"("knowledge");

-- CreateIndex
CREATE UNIQUE INDEX "Country_readiness_key" ON "Country"("readiness");
