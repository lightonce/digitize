export default defineNuxtConfig({
	css: ["/assets/styles/main.scss"],
	vite: {
		css: {
			preprocessorOptions: {
				scss: {
					additionalData: "@import '/assets/styles/variables.scss';"
				}
			}
		}
	},
	components: [
		{
			path: "~/components",
			pathPrefix: false
		},
	],
	ssr: false
});

